import React from 'react';
import ReactDOM from 'react-dom';
import List from './components/tp';
import './styles/global.scss';

const ReactRoot = document.getElementById('react-root');
ReactDOM.render(<List />, ReactRoot);
