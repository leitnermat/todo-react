import React, {Component} from 'react';
import Todolist from './Todolist';
import localStorage from 'localStorage';

// CLASS
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      items: [{
        text: 'example',
        validated: false,
      }]};
  }

  componentDidUpdate() {
    localStorage.setItem('items', JSON.stringify(this.state.items));
  }

  componentDidMount() {
    const storedItems = JSON.parse(localStorage.getItem('items'));
    const index = this.state.items.length;
    console.log(index);
    if (storedItems != null) {
      this.setState({items: storedItems});
    } else {
      this.setState({items: this.state.items});
      localStorage.setItem('items', JSON.stringify(this.state.items));
    }
  }

  handleFormSubmit = (event) => {
    const newValue = this.state.value;

    if (newValue == '' ) {
      console.log('error');
    } else {
      this.state.items.push({text: newValue, validated: false});
      console.log(this.state.items);
      this.setState({value: ''});
    }
    event.preventDefault();
  }

  handleChange = (event) => {
    this.setState({value: event.target.value});
  }

  handleDelete = () => {
    this.setState({items: []});
  }

  handleDeleteItem = (index, event) => {
    console.log('delete item: ', index);
    this.state.items.splice(index, 1);
    const newArray = this.state.items;
    this.setState({items: newArray});
    event.preventDefault();
  }

  validate = (index, item) => {
    console.log('validate', index);
    const allitems = this.state.items;
    allitems[index].validated = !item.validated;
    this.setState({
      items: allitems,
    });
    console.log(item);
  }

  render() {
    return (
      <div className="list">
        <form onSubmit={this.handleFormSubmit}>
          <input
            className="list__input"
            onChange={this.handleChange}
            name="item" placeholder="todo"
            value={this.state.value}></input>
          <button className="button button--submit">+</button>
        </form>
        <Todolist
          items={this.state.items}
          handleDeleteItem={this.handleDeleteItem}
          validate={this.validate}/>
        <button className="button button--big button--delete" onClick={this.handleDelete}>DELETE ALL</button>
      </div>
    );
  }
}
export default List;
