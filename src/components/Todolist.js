import React, {Component} from 'react';
import Items from './list';

export default class Todolist extends Component {
  render() {
    return (
      <div>
        <Items className='item'
          items={this.props.items}
          onClick={this.props.handleDeleteItem}
          validate={this.props.validate}>
        </Items>
      </div>
    );
  }
}