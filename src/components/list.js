import React from 'react';
function Todo(props) {
  return (
    props.items.sort((a, b)=>b.validated - a.validated).map((item, index) =>
      <div
        className={item.validated ? 'todo todo--done' : 'todo todo--notdone'}
        key={index}>
        {item.text}{item.validated}
        <button
          className="button button--validate"
          onClick={props.validate.bind(this, index, item)}>done</button>
        <button
          className="button button--delete"
          onClick={props.onClick.bind(this, index)}>x</button>
      </div>)
  );
}

export default Todo;